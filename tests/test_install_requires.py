# -*- coding: utf-8 -*-
# Copyright © 2015-2018 ACSONE SA/NV
# License LGPLv3 (http://www.gnu.org/licenses/lgpl-3.0-standalone.html)
import os
import unittest

from setuptools_flectra.core import (
    get_install_requires_flectra_addon,
    get_install_requires_flectra_addons,
)

from . import DATA_DIR


class TestInstallRequires(unittest.TestCase):
    """ Test the install_requires... public api """

    def test_addons_dir(self):
        r = get_install_requires_flectra_addons(DATA_DIR)
        self.assertEqual(
            set(r),
            {
                "astropy",
                # we have a mix of addons version, so two versions of Flectra
                # are pulled here (not realistic but good enough for a test)
                "flectra>=12.0a,<12.1dev",
                "flectra>=11.0a,<11.1dev",
                "flectra>=10.0,<10.1dev",
                "flectra>=8.0a,<9.0a",
                "python-dateutil",
            },
        )

    def test_addon1(self):
        addon_dir = os.path.join(DATA_DIR, "addon1")
        r = get_install_requires_flectra_addon(addon_dir)
        self.assertEqual(r, ["flectra>=8.0a,<9.0a"])

    def test_addon2(self):
        addon_dir = os.path.join(DATA_DIR, "addon2")
        r = get_install_requires_flectra_addon(addon_dir)
        self.assertEqual(
            r, ["flectra8-addon-addon1", "flectra>=8.0a,<9.0a", "python-dateutil"]
        )


if __name__ == "__main__":
    unittest.main()
