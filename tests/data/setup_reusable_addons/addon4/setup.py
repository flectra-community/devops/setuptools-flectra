import setuptools

setuptools.setup(
    setup_requires=['setuptools-flectra'],
    flectra_addon={
        'depends_override': {
            'addon1': 'flectra8-addon-addon1>=8.0.3.0.0',
        },
        'external_dependencies_override': {
            'python': {
                'astropy': 'astropy>=1.0',
            },
        },
    },
)
