setuptools-flectra
==================

.. image:: https://img.shields.io/badge/license-LGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/lgpl-3.0-standalone.html
   :alt: License: LGPL-3
.. image:: https://badge.fury.io/py/setuptools-flectra.svg
    :target: http://badge.fury.io/py/setuptools-flectra
.. image:: https://travis-ci.org/acsone/setuptools-flectra.svg?branch=master
   :target: https://travis-ci.org/acsone/setuptools-flectra
.. image:: https://coveralls.io/repos/acsone/setuptools-flectra/badge.svg?branch=master&service=github
   :target: https://coveralls.io/github/acsone/setuptools-flectra?branch=master

``setuptools-flectra`` is a library to help packaging Flectra addons with setuptools.
It mainly populates the usual ``setup.py`` keywords from the Flectra manifest files.

It enables the packaging and distribution of
Flectra addons using standard python infrastructure (ie
`setuptools <https://pypi.python.org/pypi/setuptools>`_,
`pip <https://pypi.python.org/pypi/pip>`_,
`wheel <https://pypi.python.org/pypi/wheel>`_,
and `pypi <https://pypi.python.org>`_).


Thanks to setuptools-odoo Team!!!
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

``setuptools-flectra`` is a fork from ``setuptools-odoo`` to provide the same possibilities to used python packages for flectra addons.

.. contents::

Requirements
~~~~~~~~~~~~

The following prerequisites apply:

  * Flectra version 1.0 are supported (see notes in the
    documentation for implementation differences).
  * To install addons packaged with this tool, any pip version that
    supports the wheel package format should work (ie pip >= 1.4).
  * For any advanced use such as installing from source, installing from
    git, packaging wheels etc, you need a recent version of pip (>= 9.0.1).
  * Finally, if you are using Flectra 8, 9 or 10, you need to install
    `flectra-autodiscover <https://pypi.python.org/pypi/flectra-autodiscover>`_
    (``pip install flectra-autodiscover``) to provide automatic extension
    of the addons path (and workaround a bug with setuptools > 31 and Flectra 10).
    flectra-autodiscover is *not* necessary for Flectra >= 11.

Packaging a single addon
~~~~~~~~~~~~~~~~~~~~~~~~

To be packaged with this library, the addon source code must have the
following structure (assuming the addon is named ``<addon_name>``):

  .. code::

    # Flectra >= 1.0
    setup.py
    flectra/
    flectra/addons/
    flectra/addons/<addon_name>/
    flectra/addons/<addon_name>/__manifest__.py
    flectra/addons/<addon_name>/...

where ``flectra/__init__.py``, ``flectra/addons/__init__.py``,
and ``flectra_addons/__init__.py`` are standard python namespace package
declaration ``__init__.py`` (note ``__init__.py`` is absent for Flectra >= 11):

  .. code:: python

    __import__('pkg_resources').declare_namespace(__name__)

and where setup.py has the following content:

  .. code:: python

    import setuptools

    setuptools.setup(
        setup_requires=['setuptools-flectra'],
        flectra_addon=True,
    )

The usual setup() keyword arguments are computed automatically from the
Flectra manifest file (``__manifest__.py`` or ``__openerp__.py``) and contain:

  * ``name``: the package name, ``flectra<series>-addon-<addon_name>``
  * ``version``: the ``version`` key from the manifest
  * ``description``: the ``summary`` key from the manifest if it exists otherwise
    the ``name`` key from the manifest
  * ``long_description``: the content of the ``README.rst`` file if it exists,
    otherwise the ``description`` key from the manifest
  * ``url``: the ``website`` key from the manifest
  * ``license``: the ``license`` key from the manifest
  * ``packages``: autodetected packages
  * ``namespace_packages``: ``['flectra', 'flectra.addons']`` (Flectra 10) or
    ``['flectra_addons']`` (Flectra 8, 9), absent for Flectra 11
  * ``zip_safe``: ``False``
  * ``include_package_data``: ``True``
  * ``install_requires``: dependencies to Flectra, other addons (except official
    flectra community and enterprise addons, which are brought by the Flectra dependency)
    and python libraries.
  * ``python_requires``

Then, the addon can be deployed and packaged with usual ``setup.py``
or ``pip`` commands such as:

  .. code:: shell

    python setup.py install
    python setup.py develop
    python setup.py bdist_wheel
    pip install flectra<8|9|10|11|12|13|14>-addon-<addon name>
    pip install -e .
    pip install -e git+https://github.com/OCA/<repo>/<addon>#egg=flectra<8|9|10|11|12|13|14>-addon-<addon name>\&subdirectory=setup/<addon name>

.. note::

   When using pip to install from source, the `-e` option is important
   because of `pip issue #3500 <https://github.com/pypa/pip/issues/3500>`_.
   The `-e` option has the huge advantage of letting `pip freeze` produce
   meaningful output.

When ``flectra-server-autodiscover`` is installed, The
addons-path is automatically populated with all places providing
flectra addons installed with this method.

It is of course highly recommanded to run in a virtualenv.

  .. note:: Flectra 8, 9 namespace.

     Although the addons are packaged in the ``flectra_addons`` namespace,
     the code can still import them using ``import flectra.addons....``.
     ``flectra_addons`` must never appear in the code, it is just a packaging
     peculiarity for Flectra 8 and 9 only, and does not require any change
     to the addons source code.

Packaging multiple addons
~~~~~~~~~~~~~~~~~~~~~~~~~

Addons that are intended to be reused or depended upon by other addons
MUST be packaged individually.  When preparing a project for a specific customer,
it is common to prepare a collection of addons that are not intended to be
depended upon by addons outside of the project. setuptools-flectra provides
tools to help you do that.

To be packaged with this library, your project must be structured according
to the following structure:

  .. code::

    # Flectra >= 1.0
    setup.py
    flectra/
    flectra/addons/
    flectra/addons/<addon1_name>/
    flectra/addons/<addon1_name>/__manifest__.py
    flectra/addons/<addon1_name>/...
    flectra/addons/<addon2_name>/
    flectra/addons/<addon2_name>/__manifest__.py
    flectra/addons/<addon2_name>/...

where setup.py has the following content:

  .. code:: python

    import setuptools

    setuptools.setup(
        name='<your project package name>',
        version='<your version>',
        # ...any other setup() keyword
        setup_requires=['setuptools-flectra'],
        flectra_addons=True,
    )

The following setup() keyword arguments are computed automatically from the
Flectra manifest files (``__manifest__.py`` or ``__openerp__.py``) and contain:

  * ``packages``: autodetected packages
  * ``namespace_packages``: ``['flectra', 'flectra.addons']`` (Flectra 10) or
    ``['flectra_addons']`` (Flectra 8, 9), absent for Flectra 11
  * ``zip_safe``: ``False``
  * ``include_package_data``: ``True``
  * ``install_requires``: dependencies on Flectra, any depending addon not found
    in the addons directory, and external python dependencies.
  * ``python_requires``

Controlling setuptools-flectra behaviour
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

It is possible to use a dictionary instead of ``True`` for the ``flectra_addon``
and ``flectra_addons`` keywords, in order to control their behaviour.

The following keys are supported:

  * ``depends_override``, used to precisely control flectra addons dependencies.
    Its value must be a dictionary mapping addon names to a package
    requirement string.
  * ``external_dependencies_override``, used to precisely control python
    external dependencies. Its value must be a dictionary with one ``python``
    key, with value a dictionary mapping python external dependencies to
    python package requirement strings.
  * ``flectra_version_override``, used to specify which Flectra series to use
    (8.0, 9.0, 10.0, 11.0, ...) in case an addon version does not start with the Flectra
    series number. Use this only as a last resort, if you have no way to
    correct the addon version in its manifest.
  * ``post_version_strategy_override``, used to specify how the git commits are used
    to amend the version found in the manifest (see the Versioning_ section below).

For instance, if your module requires at least version 10.0.3.2.0 of
the connector addon, as well as at least version 0.5.5 of py-Asterisk,
your setup.py would look like this:

  .. code:: python

    import setuptools

    setuptools.setup(
        setup_requires=['setuptools-flectra'],
        flectra_addon={
            'depends_override': {
                'connector': 'flectra10-addon-connector>=10.0.3.2.0',
            },
            'external_dependencies_override': {
                'python': {
                    'Asterisk': 'py-Asterisk>=0.5.5',
                },
            },
        },
    )

setuptools-flectra-make-default helper script
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Since reusable addons are generally not structured using the namespace
package but instead collected in a directory with each subdirectory containing
an addon, this package provides the ``setuptools-flectra-make-default`` script which
creates a default ``setup.py`` for each addon according to the following structure:

  .. code::

    # Flectra >= 11
    setup/
    setup/addon1/
    setup/addon1/setup.py
    setup/addon1/flectra/
    setup/addon1/flectra/addons/
    setup/addon1/flectra/addons/<addon1_name> -> ../../../../<addon1_name>
    setup/addon2/setup.py
    setup/addon1/flectra/
    setup/addon2/flectra/addons/
    setup/addon2/flectra/addons/<addon2_name> -> ../../../../<addon2_name>
    <addon1_name>/
    <addon1_name>/__manifest__.py
    <addon1_name>/...
    <addon2_name>/
    <addon2_name>/__manifest__.py
    <addon2_name>/...

    # Flectra 10
    setup/
    setup/addon1/
    setup/addon1/setup.py
    setup/addon1/flectra/
    setup/addon1/flectra/__init__.py
    setup/addon1/flectra/addons/
    setup/addon1/flectra/addons/__init__.py
    setup/addon1/flectra/addons/<addon1_name> -> ../../../../<addon1_name>
    setup/addon2/setup.py
    setup/addon1/flectra/
    setup/addon1/flectra/__init__.py
    setup/addon2/flectra/addons/
    setup/addon2/flectra/addons/__init__.py
    setup/addon2/flectra/addons/<addon2_name> -> ../../../../<addon2_name>
    <addon1_name>/
    <addon1_name>/__manifest__.py
    <addon1_name>/...
    <addon2_name>/
    <addon2_name>/__manifest__.py
    <addon2_name>/...

    # Flectra 8, 9
    setup/
    setup/addon1/
    setup/addon1/setup.py
    setup/addon1/flectra_addons/
    setup/addon1/flectra_addons/__init__.py
    setup/addon1/flectra_addons/<addon1_name> -> ../../../<addon1_name>
    setup/addon2/setup.py
    setup/addon2/flectra_addons/
    setup/addon2/flectra_addons/__init__.py
    setup/addon2/flectra_addons/<addon2_name> -> ../../../<addon2_name>
    <addon1_name>/
    <addon1_name>/__openerp__.py
    <addon1_name>/...
    <addon2_name>/
    <addon2_name>/__openerp__.py
    <addon2_name>/...

Available options::

  usage: setuptools-flectra-make-default [-h] --addons-dir ADDONS_DIR [--force]
                                      [--flectra-version-override ODOO_VERSION_OVERRIDE]
                                      [--metapackage METAPACKAGE] [--clean]
                                      [--commit]

  Generate default setup.py for all addons in an Flectra addons directory

  optional arguments:
    -h, --help            show this help message and exit
    --addons-dir ADDONS_DIR, -d ADDONS_DIR
    --force, -f
    --flectra-version-override ODOO_VERSION_OVERRIDE
                          Force Flectra version for situations where some addons
                          versions do not start with the flectra version.
    --metapackage METAPACKAGE, -m METAPACKAGE
                          Create a metapackage using the given name. This
                          package depends on all installable addons in
                          ADDONS_DIR.
    --clean, -c           Clean the setup directory: remove setups of
                          uninstallable addons, remove files corresponding to
                          other Flectra versions, remove metapackage setup if there
                          are no installable addons.
    --commit              Git commit changes, if any.

``setuptools-flectra-make-default`` is also available as a `pre-commit
<https://pre-commit.com/>`_ hook. To use it, you can add such an entry
in your `.pre-commit-config.yaml`:

.. code:: yaml

  repos:
    - repo: https://github.com/acsone/setuptools-flectra
      rev: 2.5.2
      hooks:
        - id: setuptools-flectra-make-default

setuptools-flectra-get-requirements helper script
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Since it is a common practice in the Flectra world to have a file named
``requirements.txt`` at the repository root, this script helps generating it
from the external dependencies declared in addons manifests.::

  usage: setuptools-flectra-get-requirements [-h] [--addons-dir ADDONS_DIR] [--output OUTPUT]

  Print external python dependencies for all addons in an Flectra addons directory.
  If dependencies overrides are declared in setup/{addon}/setup.py, they are
  honored in the output.

  optional arguments:
    -h, --help            show this help message and exit
    --addons-dir ADDONS_DIR, -d ADDONS_DIR
                          addons directory (default: .
    --output OUTPUT, -o OUTPUT
                          output file (default: stdout)
    --header HEADER       output file header


Versioning
~~~~~~~~~~

By default, setuptools-flectra does its best to detect if an addon has changed
compared to the version indicated in it's manifest. To this end it explores the
git log of the addon subtree.

If the last change to the addon corresponds to the version number in the manifest,
it is used as is for the python package version. Otherwise a counter
is incremented for each commit and the resulting version number includes that counter.

The default strategy depends on the Flectra series. It has the following form,
N being the number of git commits since the version change.

- Strategy ``.99.devN`` is the default for series 8 to 12 and yields
  ``[8|9|10|11|12].0.x.y.z.99.devN``.
- Strategy ``+1.devN`` is the default for series 13 and 14 and yields
  ``[13|14].0.x.y.z+1.devN``.
- Strategy ``none`` is not used by default and disables the post
  versioning mechanism, yielding the version found in the manifest.

This schemes are compliant with the accepted python versioning scheme documented
in `PEP 440 <https://www.python.org/dev/peps/pep-0440/#developmental-releases>`_.

.. Note::

  for pip to install a developmental version, it must be invoked with the --pre
  option.

Public API
~~~~~~~~~~

The ``setuptools_flectra`` package exposes a provisional public API.

* ``get_addon_metadata(addon_dir, ...)`` returns an ``email.message.Message``
  compliant with `PEP 566 -- Metadata for Python Software Packages 2.1
  <https://www.python.org/dev/peps/pep-0566/>`_.

Useful links
~~~~~~~~~~~~

- pypi page: https://pypi.python.org/pypi/setuptools-flectra
- documentation: https://setuptools-flectra.readthedocs.io
- code repository: https://github.com/acsone/setuptools-flectra
- report issues at: https://github.com/acsone/setuptools-flectra/issues
- see also flectra-autodiscover: https://pypi.python.org/pypi/flectra-autodiscover

Credits
~~~~~~~

Author:

  - Stéphane Bidoul (`ACSONE <http://acsone.eu/>`_)

Contributors

  - Benjamin Willig
    Matteo Bilotta

Many thanks to Daniel Reis who cleared the path, and Laurent Mignon who convinced
me it was possible to do it using standard Python setup tools and had the idea of
the flectra_addons namespace package.
