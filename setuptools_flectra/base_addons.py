# -*- coding: utf-8 -*-
# Copyright © 2015-2019 ACSONE SA/NV
# License LGPLv3 (http://www.gnu.org/licenses/lgpl-3.0-standalone.html)
""" List of Flectra official addons which are not released as individual packages.
They are therefore considered as installed as soon as the 'flectra' dependency
is satisfied. """

from pkg_resources import resource_string


def _addons(suffix):
    b = resource_string("setuptools_flectra", "addons-%s.txt" % suffix)
    return {a for a in b.decode("ascii").split("\n") if not a.startswith("#")}

flectra1 = _addons("1")
