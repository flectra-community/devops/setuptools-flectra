# -*- coding: utf-8 -*-
# Copyright © 2015-2018 ACSONE SA/NV
# License LGPLv3 (http://www.gnu.org/licenses/lgpl-3.0-standalone.html)

import email.parser
import io
import os
from distutils.core import DistutilsSetupError
from email.message import Message
from warnings import warn

import setuptools

from . import base_addons, external_dependencies
from .git_postversion import STRATEGY_99_DEVN, STRATEGY_P1_DEVN, get_git_postversion
from .manifest import is_installable_addon, read_manifest

FLECTRA_VERSION_INFO = {
    "1.0": {
        "flectra_dep": "flectra>=1.0,<2.0",
        "base_addons": base_addons.flectra1,
        "pkg_name_pfx": "flectra1-addon-",
        "addons_ns": "flectra.addons",
        "namespace_packages": None,
        "python_requires": ">=3.5",
        "universal_wheel": False,
        "git_postversion_strategy": STRATEGY_99_DEVN,
    },
}


def _get_flectra_version_info(addons_dir, flectra_version_override=None):
    """ Detect Flectra version from an addons directory """
    flectra_version_info = None
    addons = os.listdir(addons_dir)
    for addon in addons:
        addon_dir = os.path.join(addons_dir, addon)
        if is_installable_addon(addon_dir):
            manifest = read_manifest(addon_dir)
            _, _, addon_flectra_version_info = _get_version(
                addon_dir, manifest, flectra_version_override, git_post_version=False
            )
            if (
                flectra_version_info is not None
                and flectra_version_info != addon_flectra_version_info
            ):
                raise DistutilsSetupError(
                    "Not all addons are for the same "
                    "flectra version in %s (error detected "
                    "in %s)" % (addons_dir, addon)
                )
            flectra_version_info = addon_flectra_version_info
    return flectra_version_info


def _get_version(
    addon_dir,
    manifest,
    flectra_version_override=None,
    git_post_version=True,
    post_version_strategy_override=None,
):
    """ Get addon version information from an addon directory """
    version = manifest.get("version")
    if not version:
        warn("No version in manifest in %s" % addon_dir)
        version = "0.0.0"
    if not flectra_version_override:
        if len(version.split(".")) < 5:
            raise DistutilsSetupError(
                "Version in manifest must have at least "
                "5 components and start with "
                "the Flectra series number in %s" % addon_dir
            )
        flectra_version = ".".join(version.split(".")[:2])
    else:
        flectra_version = flectra_version_override
    if flectra_version not in FLECTRA_VERSION_INFO:
        raise DistutilsSetupError(
            "Unsupported flectra version '{}' in {}".format(flectra_version, addon_dir)
        )
    flectra_version_info = FLECTRA_VERSION_INFO[flectra_version]
    if git_post_version:
        version = get_git_postversion(
            addon_dir,
            post_version_strategy_override
            or flectra_version_info["git_postversion_strategy"],
        )
    return version, flectra_version, flectra_version_info


def _no_nl(s):
    if not s:
        return s
    return " ".join(s.split())


def _get_description(addon_dir, manifest):
    s = manifest.get("summary", "").strip() or manifest.get("name").strip()
    return _no_nl(s)


def _get_long_description(addon_dir, manifest):
    readme_path = os.path.join(addon_dir, "README.rst")
    if os.path.exists(readme_path):
        with open(readme_path) as rf:
            return rf.read()
    else:
        return manifest.get("description")


def _get_author(manifest):
    return _no_nl(manifest.get("author"))


def _get_author_email(manifest):
    author = _get_author(manifest)


def make_pkg_name(flectra_version_info, addon_name):
    name = flectra_version_info["pkg_name_pfx"] + addon_name
    return name


def make_pkg_requirement(addon_dir, flectra_version_override=None):
    manifest = read_manifest(addon_dir)
    addon_name = os.path.basename(addon_dir)
    _, _, flectra_version_info = _get_version(
        addon_dir, manifest, flectra_version_override, git_post_version=False
    )
    return make_pkg_name(flectra_version_info, addon_name)


def _get_install_requires(
    flectra_version_info,
    manifest,
    no_depends=None,
    depends_override=None,
    external_dependencies_override=None,
):
    install_requires = []
    # dependency on Flectra
    flectra_dep = flectra_version_info["flectra_dep"]
    if flectra_dep:
        install_requires.append(flectra_dep)
    # dependencies on other addons (except Flectra official addons)
    for depend in manifest.get("depends", []):
        if depend in flectra_version_info["base_addons"]:
            continue
        if no_depends and depend in no_depends:
            continue
        if depends_override and depend in depends_override:
            install_require = depends_override[depend]
        else:
            install_require = make_pkg_name(flectra_version_info, depend)
        if install_require:
            install_requires.append(install_require)
    # python external_dependencies
    for dep in manifest.get("external_dependencies", {}).get("python", []):
        if (
            external_dependencies_override
            and dep in external_dependencies_override.get("python", {})
        ):
            dep = external_dependencies_override.get("python", {})[dep]
        else:
            dep = external_dependencies.EXTERNAL_DEPENDENCIES_MAP.get(dep, dep)
        install_requires.append(dep)
    return sorted(install_requires)


def get_install_requires_flectra_addon(
    addon_dir,
    no_depends=None,
    depends_override=None,
    external_dependencies_override=None,
    flectra_version_override=None,
):
    """ Get the list of requirements for an addon """
    manifest = read_manifest(addon_dir)
    _, _, flectra_version_info = _get_version(
        addon_dir, manifest, flectra_version_override, git_post_version=False
    )
    return _get_install_requires(
        flectra_version_info,
        manifest,
        no_depends,
        depends_override,
        external_dependencies_override,
    )


def get_install_requires_flectra_addons(
    addons_dir,
    depends_override=None,
    external_dependencies_override=None,
    flectra_version_override=None,
):
    """ Get the list of requirements for a directory containing addons """
    addon_dirs = []
    addons = os.listdir(addons_dir)
    for addon in addons:
        addon_dir = os.path.join(addons_dir, addon)
        if is_installable_addon(addon_dir):
            addon_dirs.append(addon_dir)
    install_requires = set()
    for addon_dir in addon_dirs:
        r = get_install_requires_flectra_addon(
            addon_dir,
            no_depends=addons,
            depends_override=depends_override,
            external_dependencies_override=external_dependencies_override,
            flectra_version_override=flectra_version_override,
        )
        install_requires.update(r)
    return sorted(install_requires)


def _find_addons_dir():
    """Try to find the addons dir / namespace package

    Returns addons_dir, addons_ns
    """
    res = set()
    for flectra_version_info in FLECTRA_VERSION_INFO.values():
        addons_ns = flectra_version_info["addons_ns"]
        addons_dir = os.path.join(*addons_ns.split("."))
        if os.path.isdir(addons_dir):
            if not flectra_version_info["namespace_packages"] or os.path.isfile(
                os.path.join(addons_dir, "__init__.py")
            ):
                res.add((addons_dir, addons_ns))
    if len(res) == 0:
        raise RuntimeError("No addons namespace found.")
    if len(res) > 1:
        raise RuntimeError("More than one addons namespace found.")
    return res.pop()


def _make_classifiers(manifest):
    classifiers = ["Programming Language :: Python", "Framework :: Flectra"]

    # commonly used licenses in OCA
    LICENSES = {
        "agpl-3": "License :: OSI Approved :: " "GNU Affero General Public License v3",
        "agpl-3 or any later version": "License :: OSI Approved :: "
        "GNU Affero General Public License v3 or later (AGPLv3+)",
        "gpl-2": "License :: OSI Approved :: " "GNU General Public License v2 (GPLv2)",
        "gpl-2 or any later version": "License :: OSI Approved :: "
        "GNU General Public License v2 or later (GPLv2+)",
        "gpl-3": "License :: OSI Approved :: " "GNU General Public License v3 (GPLv3)",
        "gpl-3 or any later version": "License :: OSI Approved :: "
        "GNU General Public License v3 or later (GPLv3+)",
        "lgpl-2": "License :: OSI Approved :: "
        "GNU Lesser General Public License v2 (LGPLv2)",
        "lgpl-2 or any later version": "License :: OSI Approved :: "
        "GNU Lesser General Public License v2 or later (LGPLv2+)",
        "lgpl-3": "License :: OSI Approved :: "
        "GNU Lesser General Public License v3 (LGPLv3)",
        "lgpl-3 or any later version": "License :: OSI Approved :: "
        "GNU Lesser General Public License v3 or later (LGPLv3+)",
    }
    license = manifest.get("license")
    if license:
        license_classifier = LICENSES.get(license.lower())
        if license_classifier:
            classifiers.append(license_classifier)

    # commonly used development status in OCA
    DEVELOPMENT_STATUSES = {
        "alpha": "Development Status :: 3 - Alpha",
        "beta": "Development Status :: 4 - Beta",
        "production/stable": "Development Status :: 5 - Production/Stable",
        "stable": "Development Status :: 5 - Production/Stable",
        "production": "Development Status :: 5 - Production/Stable",
        "mature": "Development Status :: 6 - Mature",
    }
    development_status = manifest.get("development_status")
    if development_status:
        development_status_classifer = DEVELOPMENT_STATUSES.get(
            development_status.lower()
        )
        if development_status_classifer:
            classifiers.append(development_status_classifer)

    return classifiers


def _setuptools_find_packages(flectra_version_info):
    # setuptools.find_package() does not find namespace packages
    # without __init__.py, apparently, so work around that
    pkg = setuptools.find_packages()
    if flectra_version_info["addons_ns"] not in pkg:
        pkg.append(flectra_version_info["addons_ns"])
    return pkg


def get_addon_metadata(
    addon_dir,  # type: str
    depends_override=None,  # type: dict[str, str]
    external_dependencies_override=None,  # type: dict[str: dict[str: str]]
    flectra_version_override=None,  # type: str
    post_version_strategy_override=None,  # type: str
):
    # type: (...) -> Message
    """
    Return Python Package Metadata 2.1 for an Flectra addon as an
    email.message.Message.

    The Description field is absent and is stored in the message payload.
    All values are guaranteed to not contain newline characters, except for
    the payload. On python 3, all values are str. On python 2 the value types
    can be str or unicode depending on the python manifests.
    """
    smeta = get_addon_setuptools_keywords(
        addon_dir,
        depends_override=depends_override,
        external_dependencies_override=external_dependencies_override,
        flectra_version_override=flectra_version_override,
        post_version_strategy_override=post_version_strategy_override,
    )
    meta = Message()

    def _set(name, sname):
        svalue = smeta.get(sname)
        if svalue:
            if not isinstance(svalue, list):
                svalue = [svalue]
            for item in svalue:
                meta[name] = item

    meta["Metadata-Version"] = "2.1"
    _set("Name", "name")
    _set("Version", "version")
    _set("Requires-Python", "python_requires")
    _set("Requires-Dist", "install_requires")
    _set("Summary", "description")
    _set("Home-page", "url")
    _set("License", "license")
    _set("Author", "author")
    _set("Author-email", "author_email")
    _set("Classifier", "classifiers")
    long_description = smeta.get("long_description")
    if long_description:
        meta.set_payload(long_description)

    return meta


def get_addon_setuptools_keywords(
    addon_dir,
    depends_override=None,
    external_dependencies_override=None,
    flectra_version_override=None,
    post_version_strategy_override=None,
):
    addon_name = os.path.basename(os.path.abspath(addon_dir))
    manifest = read_manifest(addon_dir)
    if os.path.exists("PKG-INFO"):
        with io.open("PKG-INFO", encoding="utf-8") as fp:
            pkg_info = email.parser.HeaderParser().parse(fp)
            version = pkg_info["Version"]
        _, _, flectra_version_info = _get_version(
            addon_dir, manifest, flectra_version_override, git_post_version=False
        )
    else:
        version, _, flectra_version_info = _get_version(
            addon_dir,
            manifest,
            flectra_version_override,
            git_post_version=True,
            post_version_strategy_override=post_version_strategy_override,
        )
    install_requires = get_install_requires_flectra_addon(
        addon_dir,
        depends_override=depends_override,
        external_dependencies_override=external_dependencies_override,
        flectra_version_override=flectra_version_override,
    )
    setup_keywords = {
        "name": make_pkg_name(flectra_version_info, addon_name),
        "version": version,
        "description": _get_description(addon_dir, manifest),
        "long_description": _get_long_description(addon_dir, manifest),
        "url": manifest.get("website"),
        "license": manifest.get("license"),
        "packages": _setuptools_find_packages(flectra_version_info),
        "include_package_data": True,
        "namespace_packages": flectra_version_info["namespace_packages"],
        "zip_safe": False,
        "install_requires": install_requires,
        "python_requires": flectra_version_info["python_requires"],
        "author": _get_author(manifest),
        "author_email": _get_author_email(manifest),
        "classifiers": _make_classifiers(manifest),
    }
    # import pprint; pprint.pprint(setup_keywords)
    return {k: v for k, v in setup_keywords.items() if v is not None}


def prepare_flectra_addon(
    depends_override=None,
    external_dependencies_override=None,
    flectra_version_override=None,
):
    addons_dir, addons_ns = _find_addons_dir()
    potential_addons = os.listdir(addons_dir)
    # list installable addons, except auto-installable ones
    # in case we want to combine an addon and it's glue modules
    # in a package named after the main addon
    addons = [
        a
        for a in potential_addons
        if is_installable_addon(
            os.path.join(addons_dir, a), unless_auto_installable=True
        )
    ]
    if len(addons) == 0:
        # if no addon is found, it may mean we are trying to package
        # a single module that is marked auto-install, so let's try
        # listing all installable modules
        addons = [
            a
            for a in potential_addons
            if is_installable_addon(os.path.join(addons_dir, a))
        ]
    if len(addons) != 1:
        raise DistutilsSetupError(
            "%s must contain exactly one "
            "installable Flectra addon dir, found %s"
            % (os.path.abspath(addons_dir), addons)
        )
    addon_name = addons[0]
    addon_dir = os.path.join(addons_dir, addon_name)
    return get_addon_setuptools_keywords(
        addon_dir,
        depends_override,
        external_dependencies_override,
        flectra_version_override,
    )


def prepare_flectra_addons(
    depends_override=None,
    external_dependencies_override=None,
    flectra_version_override=None,
):
    addons_dir, addons_ns = _find_addons_dir()
    flectra_version_info = _get_flectra_version_info(addons_dir, flectra_version_override)
    install_requires = get_install_requires_flectra_addons(
        addons_dir,
        depends_override=depends_override,
        external_dependencies_override=external_dependencies_override,
        flectra_version_override=flectra_version_override,
    )
    setup_keywords = {
        "packages": _setuptools_find_packages(flectra_version_info),
        "include_package_data": True,
        "namespace_packages": flectra_version_info["namespace_packages"],
        "zip_safe": False,
        "install_requires": install_requires,
        "python_requires": flectra_version_info["python_requires"],
    }
    # import pprint; pprint.pprint(setup_keywords)
    return {k: v for k, v in setup_keywords.items() if v is not None}
