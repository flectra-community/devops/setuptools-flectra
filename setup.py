# -*- coding: utf-8 -*-
# Copyright © 2015-2017 ACSONE SA/NV
# License LGPLv3 (http://www.gnu.org/licenses/lgpl-3.0-standalone.html)

import io
import os

import setuptools

here = os.path.abspath(os.path.dirname(__file__))

long_description = []
with io.open(os.path.join("README.rst"), encoding="utf-8") as f:
    long_description.append(f.read())
with io.open(os.path.join("CHANGES.rst"), encoding="utf-8") as f:
    long_description.append(f.read())


setuptools.setup(
    name="setuptools-flectra",
    use_scm_version=True,
    description="A library to help package Flectra addons with setuptools",
    long_description="\n".join(long_description),
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: " "GNU Lesser General Public License v3 (LGPLv3)",
        "Operating System :: POSIX",  # because we use symlinks
        "Programming Language :: Python",
        "Framework :: Flectra",
    ],
    license="LGPLv3",
    author="ACSONE SA/NV",
    author_email="info@acsone.eu",
    url="http://gitlab.com/jamotion/setuptools-flectra",
    packages=["setuptools_flectra"],
    include_package_data=True,
    install_requires=["setuptools", "setuptools_scm>=2.1,!=4.0.0"],
    python_requires=">=2.7, !=3.0.*, !=3.1.*, !=3.2.*, !=3.3.*, !=3.4.*",
    setup_requires=["setuptools-scm!=4.0.0"],
    test_suite="tests",
    entry_points={
        "console_scripts": [
            "setuptools-flectra-make-default=" "setuptools_flectra.make_default_setup:main",
            "setuptools-flectra-get-requirements=" "setuptools_flectra.get_requirements:main",
        ],
        "distutils.setup_keywords": [
            "flectra_addon = setuptools_flectra.setup_keywords:flectra_addon",
            "flectra_addons = setuptools_flectra.setup_keywords:flectra_addons",
        ],
    },
)
